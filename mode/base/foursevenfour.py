from . import general as g
from .general import Error, ERR_BAD_VALUE, ERR_INTERNAL
from .mutator import Mutator, TYPE_SBOX, TYPE_INV, TYPE_SHIFT
from .keygen import KeyGen

class FourSevenFour:

  _BIT_SIZE = 128
  _INPUT_SIZE = _BIT_SIZE//8
  _FEISTEL_BLOCK_SIZE = _INPUT_SIZE//2
  _P_BOX = [
    57, 49, 41, 33, 25, 17, 9, 1, 121, 113, 105, 97, 89, 81, 73, 65,
    61, 53, 45, 37, 29, 21, 13, 5, 125, 117, 109, 101, 93, 85, 77, 69,
    56, 48, 40, 32, 24, 16, 8, 0, 120, 112, 104, 96, 88, 80, 72, 64,
    60, 52, 44, 36, 28, 20, 12, 4, 124, 116, 108, 100, 92, 84, 76, 68,
    55, 47, 39, 31, 23, 15, 7, 127, 119, 111, 103, 95, 87, 79, 71, 63,
    59, 51, 43, 35, 27, 19, 11, 3, 123, 115, 107, 99, 91, 83, 75, 67,
    54, 46, 38, 30, 22, 14, 6, 126, 118, 110, 102, 94, 86, 78, 70, 62,
    58, 50, 42, 34, 26, 18, 10, 2, 122, 114, 106, 98, 90, 82, 74, 66
  ]

  def __init__(self):
    self._assert_self()
  
  def __call__(self, input_block, key, mode):
    try:
      if mode not in ['encrypt', 'decrypt']:
        raise Error(ERR_BAD_VALUE, "Invalid mode of operation")
    except Error as e:
      e.print()
    return self.loop(input_block, key, mode)     

  def _assert_input(self, _input):
    try:
      if len(_input) != self._INPUT_SIZE:
        raise Error(ERR_BAD_VALUE, "Input size is invalid")
      else:
        for row in _input:
          if len(row) != self._INPUT_SIZE:
            raise Error(ERR_BAD_VALUE, "Input size is invalid")
    except Error as e:
      e.print()

  def _assert_self(self):
    try:
      if len(self._P_BOX) != self._BIT_SIZE:
        raise Error(ERR_INTERNAL, 'IP size does not match')
    except Error as e:
      e.print()

  def adjust_input(self, input_block):
    return g.int_to_bit(input_block)

  def f_function(self, input_block, key):
    mod = sum(key) % 6
    mutator_choice = [
      [TYPE_SBOX, TYPE_INV, TYPE_SHIFT],
      [TYPE_INV, TYPE_SBOX, TYPE_SHIFT],
      [TYPE_SHIFT, TYPE_INV, TYPE_SBOX],
      [TYPE_SBOX, TYPE_SHIFT, TYPE_INV],
      [TYPE_INV, TYPE_SHIFT, TYPE_SBOX],
      [TYPE_SHIFT, TYPE_SBOX, TYPE_INV]
    ]

    mutator = Mutator()
    inc = 0
    output_block = input_block
    while inc < len(mutator_choice[mod]):
      output_block = mutator(output_block, mutator_choice[mod][inc], mod)
      inc+=1
    
    return output_block

  def feistel_sequence(self, left_block, right_block, key):
    mod_block = self.f_function(right_block, key)
    alt_left_block = []
    for i in range(0, self._FEISTEL_BLOCK_SIZE):
      alt_left_block.append(left_block[i] ^ mod_block[i])

    return right_block, alt_left_block

  def initial_permutation(self, input_block):
    input_block = self.adjust_input(input_block)
    output_block = g.empty_list(self._BIT_SIZE)

    for x in range(0, self._BIT_SIZE):
      px = self._P_BOX[x]
      output_block[x] = input_block[px]

    output_block = self.normalize_input(output_block)
    return output_block

  def initial_permutation_inv(self, input_block):
    input_block = self.adjust_input(input_block)
    output_block = g.empty_list(self._BIT_SIZE)

    for x in range(0, self._BIT_SIZE):
      px = self._P_BOX[x]
      output_block[px] = input_block[x]

    output_block = self.normalize_input(output_block)
    return output_block

  def loop(self, input_block, key, mode, amount=32):
    block = self.initial_permutation(input_block)
    block = g.chunk_list(block, self._FEISTEL_BLOCK_SIZE)

    left_block = block[0]
    right_block = block[1]

    keygen = KeyGen()
    int_key = keygen(key)

    for i in range(0, amount):
      if mode == 'encrypt':
        left_block, right_block = self.feistel_sequence(left_block, right_block, int_key[i])
      elif mode == 'decrypt':
        right_block, left_block = self.feistel_sequence(right_block, left_block, int_key[amount-i-1])

    output_block = left_block + right_block
    output_block = self.initial_permutation_inv(output_block)

    return output_block

  def normalize_input(self, input_block):
    return g.bit_to_int(input_block)

if __name__ == '__main__':
  # a = []
  # for x in range(0, 128):
  #   a.append(x)

  # print('a: ', a)

  f = FourSevenFour()
  list_input = [97,98,99,100,97,98,99,100,97,98,99,100,97,98,99,100]
  key = 'kucing'

  list_result = f(list_input,key,'encrypt')

  print(list_result)
  
  bismillah = f(list_result,key,'decrypt')

  print(bismillah)

  # print(list_result)

  # b = f.initial_permutation(a)

  # print('b: ', b)

  # c = f.initial_permutation_inv(b)

  # print('c: ', c)

  # c = [1, 2, 3, 5, 6, 7]

  # for x in range(1, len(c)):
  #   if c[x] - c[x-1] != 1:
  #     print('salah!')