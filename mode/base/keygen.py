from .mutator import Mutator, TYPE_SBOX, TYPE_INV, TYPE_SHIFT

class KeyGen(object):

    def __init__(self):
        self.len_block = 16
        self.iterate = 32
    
    def _get_len_block(self):
        return self.len_block
    
    def _get_iterate(self):
        return self.iterate
    
    def __call__(self, key_ex):
        list_key_ex = list(map(ord, list(key_ex)))
        # Duplicate key_ex if length key_ex < len_block
        length_key_ex = len(list_key_ex)
        if length_key_ex < self._get_len_block():
            for i in range(self._get_len_block()-length_key_ex):
                element_key_ex = list_key_ex[i%length_key_ex]
                list_key_ex.append(element_key_ex)
        
        mutator = Mutator()
        list_key_in = []
        inc = 0
        while inc < self._get_iterate():
            mod = sum(list_key_ex) % 3
            list_key = mutator(list_key_ex, mod, mod)
            list_key_in.append(list_key[:(len(list_key)//2)])               # Odd
            list_key_in.append(list_key[(len(list_key)//2):len(list_key)])    # Even
            list_key_ex = list_key
            inc+=1
        
        return list_key_in
        
if __name__ == '__main__':
    key = 'kucing1234'
    
    keygen = KeyGen()
    list_key = keygen(key)
    print(list_key)