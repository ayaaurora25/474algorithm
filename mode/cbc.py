from base.foursevenfour import FourSevenFour

import numpy as np
import os
# os.chdir(r'/Users/Untitled/Documents/ARUM/Akademik/Semester_8/IF4020 Kriptografi/UTS1/474algorithm/mode')

class CipherBlockChaining(object):

    def __init__(self):
        self.len_block = 16
   
    def _get_len_block(self):
        return self.len_block

    def read_file(self, filename):
        data = None
        with open(filename, 'r') as file:
            data = file.read()
        return data

    def write_file(self, filename, data):
        with open(filename, 'w') as file:
            file.write(data)

    def __call__(self, filename, key, proc):
        f = FourSevenFour()
        text = self.read_file(filename)
        list_block = self.chunk_block(text)  
        
        if proc == 'encrypt':
            print(proc)

            inc = 0
            cipher_block = []
            while inc < len(list_block):
                if inc == 0:
                    np.random.seed(self._get_seed_number(key))
                    initial_vector = np.random.choice(255, size=(16)).tolist()
                    cipher_block_temp = []
                    for i in range(len(initial_vector)):
                        cipher_block_temp.append(initial_vector[i] ^ list_block[inc][i])
                else:
                    cipher_block_temp = []
                    for i in range(len(cipher_block[inc-1])):
                        cipher_block_temp.append(cipher_block[inc-1][i] ^ list_block[inc][i])
                cipher_block_temp = f(cipher_block_temp, key, 'encrypt')
                cipher_block.append(cipher_block_temp)
                inc+=1
            
            # TO-DO define encryption function here, run function for len(list_block) times

            # Suppose to be result of encryption function

            list_result = [item for sublist in cipher_block for item in sublist]
            list_result = list(map(chr, list_result))

        elif proc == 'decrypt':
            print(proc)

            inc = 0
            plain_block = []
            while inc < len(list_block):
                list_block_temp = f(list_block[inc], key, 'decrypt')

                if inc == 0:
                    np.random.seed(self._get_seed_number(key))
                    initial_vector = np.random.choice(255, size=(16)).tolist()
                    plain_block_temp = []
                    for i in range(len(initial_vector)):
                        plain_block_temp.append(initial_vector[i] ^ list_block_temp[i])
                else :
                    plain_block_temp = []
                    for i in range(len(list_block[inc-1])):
                        plain_block_temp.append(list_block[inc-1][i] ^ list_block_temp[i])
                
                plain_block.append(plain_block_temp)
                inc+=1
            
            # TO-DO define decryption function here, run function for len(list_block) times

            # Suppose to be result of decryption function
       
            list_result = [item for sublist in plain_block for item in sublist]
            list_result = [item for item in list_result if item != 0]
            list_result = list(map(chr, list_result))

        self.write_file('output.txt', ''.join(list_result))
        

    def chunk_block(self, plaintext):
        list_char = list(plaintext)
        list_int = list(map(ord, list_char))
        mod = len(list_int) % self._get_len_block()

        # Padding with 0 if mod < len block
        if (mod != 0):
            for inc in range(self._get_len_block() - mod):
                list_int.append(0)
        
        list_block = [list_int[x:x+16] for x in range(0, len(list_int), 16)]

        return list_block
        
    def _get_seed_number(self, key):
        seed_number = 0
        for char in key :
            seed_number = seed_number + ord(char)
        return seed_number


if __name__ == '__main__':
    cbc = CipherBlockChaining()
    # cipherresult = cbc('input.txt', 'helo','encrypt')
    plainresult = cbc('output.1.txt', 'helo','decrypt')
    