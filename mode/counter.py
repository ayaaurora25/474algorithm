from base.foursevenfour import FourSevenFour

import numpy as np
import os
os.chdir(r'/Users/Untitled/Documents/ARUM/Akademik/Semester_8/IF4020 Kriptografi/UTS1/474algorithm/mode')

class Counter(object):
    def __init__(self):
        self.len_block = 16
    
    def _get_len_block(self):
        return self.len_block

    def read_file(self, filename):
        data = None
        with open(filename, 'r') as file:
            data = file.read()
        return data

    def write_file(self, filename, data):
        with open(filename, 'w') as file:
            file.write(data)
    
    def __call__(self, filename, key, proc):
        f = FourSevenFour()
        text = self.read_file(filename)
        np.random.seed(self._get_seed_number(key))
        initial_vector = np.random.choice(255, size=(16)).tolist()

        list_block = self.chunk_block(text)

        if proc == 'encrypt':
            print(proc)
            inc = 0
            counter = initial_vector
            list_result = []
            while inc < len(list_block):
                list_xor = []
            # TO-DO define encryption function here
                list_encrypted = f(counter, key, 'encrypt')
            #======================================
                list_xor = [list_encrypted[i] ^ list_block[inc][i] for i in range(len(list_encrypted))]
                list_result.append(list_xor)
                
                counter = self.increment_counter(counter)

                inc+=1

            list_result = [item for sublist in list_result for item in sublist]
            list_result = list(map(chr, list_result))
            
        elif proc == 'decrypt':
            print(proc)
            inc = 0
            counter = initial_vector
            list_result = []
            while inc < len(list_block):
                list_xor = []
            # TO-DO define decryption function here
                list_decrypted = f(counter, key, 'encrypt')
            #======================================
                list_xor = [list_decrypted[i] ^ list_block[inc][i] for i in range(len(list_decrypted))]
                list_result.append(list_xor)
                
                counter = self.increment_counter(counter)

                inc+=1

            list_result = [item for sublist in list_result for item in sublist]
            list_result = [item for item in list_result if item != 0]
            list_result = list(map(chr, list_result))
        
        self.write_file('output.txt', ''.join(list_result))
                
    def increment_counter(self, counter):
        idx = len(counter)-1
        is_added = False
        while idx >= 0 and is_added == False:
            if (is_added == False) and (counter[idx] < 255):
                counter[idx]+=1
                is_added = True
                
            if is_added == False and idx == 0 and counter[idx] == 255:
                for i in range(len(counter)):
                    counter[i] = 0
                is_added = True
            idx-=1
        
        return counter

    def chunk_block(self, plaintext):
        list_char = list(plaintext)
        list_int = list(map(ord, list_char))
        mod = len(list_int) % self._get_len_block()

        # Padding with 0 if mod < len block
        if (mod != 0):
            for inc in range(self._get_len_block() - mod):
                list_int.append(0)
        
        list_block = [list_int[x:x+16] for x in range(0, len(list_int), 16)]

        return list_block

    def _get_seed_number(self, key):
        seed_number = 0
        for char in key :
            seed_number = seed_number + ord(char)
        return seed_number

if __name__ == '__main__':
    count = Counter()
    # cipherresult = count('input.txt', 'helo','encrypt')
    plainresult = count('output.txt', 'helo','decrypt')
