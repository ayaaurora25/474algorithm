from base.foursevenfour import FourSevenFour

class ElectronicCodeBook(object):
    
    def __init__(self):
        self.len_block = 16
        
    def read_file(self, filename):
        data = None
        with open(filename, 'r') as file:
            data = file.read()
        return data

    def write_file(self, filename, data):
        with open(filename, 'w') as file:
            file.write(data)
    
    def __call__(self, filename, key, proc):
        f = FourSevenFour()
        text = self.read_file(filename)
        list_block = self.chunk_block(text)
        output_block = []

        if proc == 'encrypt':
            # TO-DO define encryption function here, run function for len(list_block) times
            print(proc)
            for b in list_block:
                block = f(b, key, 'encrypt')
                output_block.append(block)
        elif proc == 'decrypt':
            # TO-DO define decryption function here, run function for len(list_block) times
            print(proc)
            for b in list_block:
                block = f(b, key, 'decrypt')
                output_block.append(block)

        # Suppose to be result of encryption function
        list_result = [item for sublist in output_block for item in sublist]
        list_result = [item for item in list_result if item != 0]
        list_result = list(map(chr, list_result))
        self.write_file('output.txt', ''.join(list_result))

    def _get_len_block(self):
        return self.len_block

    def chunk_block(self, plaintext):
        list_char = list(plaintext)
        list_int = list(map(ord, list_char))
        mod = len(list_int) % self._get_len_block()

        # Padding with 0 if mod < len block
        if (mod != 0):
            for inc in range(self._get_len_block() - mod):
                list_int.append(0)
        
        list_block = [list_int[x:x+16] for x in range(0, len(list_int), 16)]

        return list_block     


if __name__ == '__main__':
    ecb = ElectronicCodeBook()
    plaintext = 'abcdabcdabcdabcdabcdabcdabcd'
    # list_block=ecb.chunk_block(plaintext)
    print(ecb('output.1.txt', 'JUSTICE','decrypt'))
    # print(len(plaintext))
    # print(list_block)
    # print(len(list_block[0]))
    # list_cipher = ecb.encrypt(plaintext)
    # print(list_cipher)
    # print(len(list_cipher))
    # print('Hello World')